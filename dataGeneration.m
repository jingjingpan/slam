% This is a script that simulates a robot random-walking in a 2-D environment.
% The only measurement/observation is the distance from the robot and the N landmarks
% at each time step.
% In this script, only the range readings are perturbed by Gaussian noise.

% Jingjing Pan 5/16/2017


clear all
close all

% Parameters taken from section 4.1 of the range-only SLAM paper
N = 6; % number of landmarks
T = 500; % number of time steps
perturbation = 0.01; % ratio of the variance of range readings to the range readings

rwStd = 10; % Standard deviation of each step of random walk

% Randomly generate landmark positions
landmarkStd = 100;
mx = normrnd(0, landmarkStd, [N, 1]);
my = normrnd(0, landmarkStd, [N, 1]);

% Preallocate robot position arrays
x = zeros(T, 1);
y = zeros(T, 1);

% Preallocate range reading matrix
Y = zeros(N, T);

% Assume random walk in the 2-D environment
for t = 1:T
	dx = normrnd(0, rwStd);
	dy = normrnd(0, rwStd);

	if t == 1
		% Assume robot starts from (0,0) at t=0
		x_t = dx;
		y_t = dy;
	else
		x_t = x(t-1) + dx;
		y_t = y(t-1) + dy;
	end
	
	rangeReal_t = sqrt((mx - x_t).^2 + (my - y_t).^2);
	rangeReading_t = normrnd(rangeReal_t, rangeReal_t * perturbation, [N, 1]);

	x(t) = x_t;
	y(t) = y_t;
	Y(:,t) = 1/2 * rangeReading_t.^2; % produce matrix Y in the range-only SLAM paper
end

fig = figure;
plot(mx, my, 'dk')
hold on
plot(x, y, '.r')
plot(x, y, '-b')